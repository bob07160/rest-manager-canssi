# Rest Manager PHP

interface to send HTTP request.

## Use

Include autoloader & classes. You are not forced to use Credentials.
Include this just if needed authentification.

```mermaid
graph LR;
  Request-->Repository;
  Credentials-->Repository;
  Repository-->Method;
  Method-->Index;
```


```php
require 'Src/Autoloader.php';

use Src\Requests\Request;
use Src\Methods\Method;
use Src\Auth\Credentials;
```

**Example without Authentification**

```php
$rest = new Method( new Request );
$data = $rest->send( 'GET', 'https://randomuser.me/api/' );
```

To see result you can use Debug class

```php
Debug::dd($data);
```


**Example with authentification**

Use the denpendency injection to use authentification

```php
$credentials = [ 'email' => 'jos@mail.com',
                 'password' => 'secret',
                 'api_token' => '123'];
        
$rest = new Method( new Request, new Credentials );
$data =  $request->send( 'POST', 'http://127.0.0.1:8000/api/login', 'http', null, $credentials );
```
### Request

If you want see the current request, use the Debug class

```php
Debug::var($rest->request);
```

To modify the request you can use public setters & getters

```php
$rest->request->setApiUrl($url);
```

### Credentials

this part is like Request. Use getters & setters

```php
$rest->auth->getAuth();
```
## Custom (if you want custom this package)

**Add new or extends Request (& Credentials if needed)** 

```mermaid
graph LR;
  Request-->CustomOrExtendsRequest;
  Credentials-->CustomOrExtendsCredentials;
  CustomOrExtendsRequest-->Repository;
  CustomOrExtendsCredentials-->Repository;
  Repository-->Method;
  Method-->Index;
```

**Inject an instance of your classes when instanciate Method class**

```php
require 'Src/Autoloader.php';

use Src\Requests\CustomOrExtendsRequest;
use Src\Auth\CustomOrExtendsCredentials;
use Src\Methods\Method;

$rest = new Method( new CustomOrExtendsRequest, new CustomOrExtendsCredentials );
$data = $rest->send( $method, $url, $scheme, $data, $credentials );
```
