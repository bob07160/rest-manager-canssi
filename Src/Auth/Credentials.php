<?php

namespace Src\Auth;

class Credentials {
    
    /**
     * Authentification array
     * @var array 
     */
    private $credentials;
    
    /** Getters */
    public function getAuth() {
        return $this->credentials;
    }

    /** Setters */
    public function setAuth($credentials) {
        $this->credentials = $credentials;
    }
    
}

