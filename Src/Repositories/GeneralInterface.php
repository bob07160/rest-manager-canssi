<?php
namespace Src\Repositories;

interface GeneralInterface {
    
    /**
     * Add properties to Request
     * @param string $scheme
     * @param string $url
     * @param array $data
     * @return $this
     */
    public function constructRequest( $scheme, $url, $data );
    
    /**
     * Add credentials to Request
     * @param array $credentials
     * @return $this
     */
    public function withCredentials( $credentials );
    
    /**
     * Create HTTP header
     * @return $this
     */
    public function createHeader();
    
    /**
     * Create flux context
     * @return $this
     */
    public function createContext();
    
    /**
     * Send request
     * @return void
     */
    public function sendRequest();
    
    /**
     * Check if https
     * @return $this
     */
    public function checkIfHttps();
    
    /**
     * Json to Array
     * @return void
     */
    public function toArray();
    
}

