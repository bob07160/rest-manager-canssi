<?php

namespace Src\Repositories;

use Src\Repositories\GeneralInterface;

class GeneralRepository implements GeneralInterface  {
    
    /**
     * @var \Requests\Request 
     */
    public $request;
    
    /**
     * @var \Auth\Credentials
     */
    protected $auth;
    
    /**
     * Construct new Request & Credentials if needed
     * @param \Requests\Request $request
     */
    public function __construct( $request, $auth = null ) {
        $this->request = $request;
        ( !is_null($auth) ) ? $this->auth = $auth : $this->auth = false;
    }
    
    /**
     * Add properties to Request
     * @param string $scheme
     * @param string $url
     * @param array $data
     * @return $this
     */
    public function constructRequest( $scheme, $url, $data = null ) {
        
        $this->request->setHost( $_SERVER['HTTP_HOST'] );
        $this->request->setApiUrl($url);
        $this->request->setScheme($scheme);
        if( !is_null($data) ) $this->request->setRequestData ($data);
        
        return $this;
    }
    
    /**
     * Add credentials to Request
     * @param array $credentials
     * @return $this
     */
    public function withCredentials( $credentials ) {
        
        $this->auth->setAuth($credentials);
        $this->request->setAuth($credentials);
        
        return $this;
    }
    
    /**
     * Create HTTP header
     * @return $this
     */
    public function createHeader() {
        $header  = "";
        $header .= "Host: ". $this->request->getHost() ."\r\n";
        $header .= "Content-type: application/x-www-form-urlencoded\r\n";
        
        $this->request->setHeader( $header );
        
        return $this;
    }
    
    /**
     * Create flux context
     * @return $this
     */
    public function createContext() {
        
        $data        = $this->request->getRequestData();
        $credentials = $this->request->getAuth();
        
        if ( is_null($data) && is_null($credentials) ) {
            
            $params = [ $this->request->getScheme() => [
                                        'method'  => $this->request->getMethod(),
                                        'header'  => $this->request->getHeader(),
                ] ];

            $this->request->setContext( stream_context_create( $params ) );
            
            return $this;
            
        } elseif ( is_null($data) && !is_null($credentials) ) {

            $content = http_build_query( $credentials );
            $params  = [ $this->request->getScheme() => [
                                        'method'  => $this->request->getMethod(),
                                        'header'  => $this->request->getHeader(),
                                        'content' => $content
                ] ];

            $this->request->setContext( stream_context_create( $params ) );
            
            return $this;
            
        } elseif ( !is_null($data) && !is_null($credentials) ) {
            
            $data    = array_merge($data, $credentials);
            $content = http_build_query( $data );
            $params  = [ $this->request->getScheme() => [
                                        'method'  => $this->request->getMethod(),
                                        'header'  => $this->request->getHeader(),
                                        'content' => $content
                ] ];

            $this->request->setContext( stream_context_create( $params ) );
            
            return $this;
            
        } else {
            
            $content = http_build_query( $data );
            $params  = [ $this->request->getScheme() => [
                                        'method'  => $this->request->getMethod(),
                                        'header'  => $this->request->getHeader(),
                                        'content' => $content
                ] ];

            $this->request->setContext( stream_context_create( $params ) );
            
            return $this;
        }
    }
    
    /**
     * Send request
     * @return void
     */
    public function sendRequest() {
        $apiUrl  = $this->request->getApiUrl();
        $context = $this->request->getContext();
        
        try {
            $response = file_get_contents($apiUrl, false, $context);
        } catch ( Exception $e ) {
            return $e->getMessage();
        }
        
        $this->request->setResponseData( $response );
        $this->toArray( $this->request->getResponseData() );
        
        $this->request->getResponseData();
        
        return $this;
    }
    
    /**
     * Check if https
     * @return $this
     */
    public function checkIfHttps() {
        $parse_url = parse_url( $this->request->getApiUrl() );

        $https = ( 'https' === $parse_url['scheme'] ) ? true : false ;

        $this->request->setHttps( $https );
        
        return $this;
    }
    
    /**
     * Json to Array
     * @return void
     */
    public function toArray() {
        $response = $this->request->getResponseData();
        
        $result = json_decode( $response );
        
        return $this->request->setResponseData( $result );
        
    }
}
