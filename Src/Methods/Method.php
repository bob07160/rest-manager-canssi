<?php

namespace Src\Methods;

use Src\Requests\Request;
use Src\Repositories\GeneralRepository;
use Src\Auth\Credentials;

class Method {
    
    /**
     * Authentification array
     * @var array 
     */
    protected $auth;
    
    /**
     * @var Src\Requests\Request 
     */
    public $request;
    
    /**
     * @var Src\Repositories\GeneralRepository 
     */
    public $repository;
    
    /**
     * Construc a new method with an instance of Request (& Credentials if needed)
     * @param Src\Requests\Request $request
     * @param Src\Auth\Credentials $auth
     */
    public function __construct( $request, $auth = null ) {
        ( is_null($auth) ) ? 
                $this->repository = new GeneralRepository( $request ) :
                    $this->auth = $auth && $this->repository = new GeneralRepository( $request, $auth );
        
        $this->request = $request;
    }
    
    /**
     * Send a new HTTP request
     * @param string $method GET|POST|PUT|DELETE|OTHER
     * @param string $url
     * @param string $scheme = 'https
     * @param array|null $data = null
     * @param array|bool $withCredentials = null
     * @return array response
     */
    public function send( $method, $url, $scheme = 'https', $data = null, $withCredentials = null ) {
        
        $this->request->setMethod( $method );
        
        if ( is_null($withCredentials) ) {
            $this->repository->constructRequest( $scheme, $url, $data )
                             ->checkIfHttps()
                             ->createHeader()
                             ->createContext()
                             ->sendRequest();
            
            return $this->request->getResponseData();
            
        } else {
            $this->repository->constructRequest( $scheme, $url, $data )
                             ->withCredentials( $withCredentials )
                             ->checkIfHttps()
                             ->createHeader()
                             ->createContext()
                             ->sendRequest();
            
            return $this->request->getResponseData();
        }
    }
}
