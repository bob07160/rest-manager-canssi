<?php

class Autoloader {
    
    /**
     * call autload
     */
    static function register() {
        spl_autoload_register( array(__CLASS__, 'autoload') );
    }
    
    /**
     * load classes & replace \ => /
     * @param string $class_name
     */
    static function autoload($class_name) {
        $class_name = str_replace("\\", '/', $class_name);
        require $class_name .'.php';
    }
}
Autoloader::register();
