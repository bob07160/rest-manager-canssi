<?php

namespace Src\Requests;

/**
 * Composants d'une requete
 */
class Request {
    
    /**
     * Authentification array
     * @var array 
     */
    private $auth;
    
    /**
     * HTTP HOST
     * @var string 
     */
    private $host;
    
    /**
     * Target url
     * @var string 
     */
    private $apiUrl;
    
    /**
     * HTTPS | HTTP
     * @var string 
     */
    private $scheme;
    
    /**
     * GET | POST | PUT | DELETE
     * @var string 
     */
    private $method;
    
    /**
     * Content-Type
     * @var string 
     */
    private $headerOptions;
    
    /**
     * true if HTTPS
     * @var bool 
     */
    private $https;
    
    /**
     * Content-type: application/x-www-form-urlencoded
     * @var string 
     */
    private $header;
    
    /**
     * Flux context
     * @var string 
     */
    private $context;
    
    /**
     * Request body
     * @var mixed
     */
    private $requestData;
    
    /**
     * Response
     * @var array|bool 
     */
    private $responseData;
    
    /** GETTERS */
    public function getAuth() {
        return $this->auth;
    }

    public function getHttps() {
        return $this->https;
    }

    public function getHost() {
        return $this->host;
    }

    public function getApiUrl() {
        return $this->apiUrl;
    }

    public function getScheme() {
        return $this->scheme;
    }

    public function getHeader() {
        return $this->header;
    }

    public function getMethod() {
        return $this->method;
    }
    
    public function getContext() {
        return $this->context;
    }

    public function getRequestData() {
        return $this->requestData;
    }

    public function getResponseData() {
        return $this->responseData;
    }
    
    /** SETTERS */
    public function setAuth($auth) {
        $this->auth = $auth;
    }
    
    // Setters
    public function setHost($host) {
        $this->host = $host;
    }
    
    public function setHttps($https) {
        $this->https = $https;
    }
    
    public function setApiUrl($apiUrl) {
        $this->apiUrl = $apiUrl;
    }

    public function setScheme($scheme) {
        $this->scheme = $scheme;
    }

    public function setHeader($header) {
        $this->header = $header;
    }
    
    public function setContext($context) {
        $this->context = $context;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function setRequestData($requestData) {
        $this->requestData = $requestData;
    }

    public function setResponseData($responseData) {
        $this->responseData = $responseData;
    }
    
}

