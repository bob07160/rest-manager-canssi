<?php
require 'Src/Autoloader.php';

use Src\Requests\Request;
use Src\Methods\Method;
use Src\Auth\Credentials;

/**
 * This part is just an example,
*/

// Without Authentification
$request = new Method( new Request );
$data = $request->send( 'GET', 'https://randomuser.me/api/' );

// You can use Debug class to see results
Debug::dd($data);

// With Authentification
$credentials = [ 'email' => 'jos@mail.com',
                 'password' => 'secret',
                 'api_token' => '123'];
        
$request = new Method( new Request, new Credentials );
$data =  $request->send( 'POST', 'http://127.0.0.1:8000/api/login', 'http', null, $credentials );

Debug::dd($data);

